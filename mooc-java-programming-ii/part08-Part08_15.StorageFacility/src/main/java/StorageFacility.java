/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.HashMap;
import java.util.ArrayList;

public class StorageFacility {

    private HashMap<String, ArrayList<String>> storage;

    public StorageFacility() {
        this.storage = new HashMap<>();
    }

    public void add(String unit, String item) {
        this.storage.putIfAbsent(unit, new ArrayList<>());
        this.storage.get(unit).add(item);
    }

    public ArrayList<String> contents(String unit) {
        return this.storage.getOrDefault(unit, new ArrayList<>());
    }

    public void remove(String unit, String item) {
        ArrayList<String> storageUnit = storage.getOrDefault(unit, new ArrayList<>());

//        for (int i = 0; i < storageUnit.size(); i++) {
//            if (storageUnit.get(i).equals(item)) {
//                storageUnit.remove(i);
//                
//            }
//        }
        storageUnit.remove(item);

        //remove unit if empty
        if (storageUnit.isEmpty()) {
            storage.remove(unit);
        }
    }

    public ArrayList<String> storageUnits() {
        ArrayList<String> nonEmptyUnits = new ArrayList<>();

        storage.forEach((unit, items) -> {
            if (!items.isEmpty()) {
                nonEmptyUnits.add(unit);
            }
        });
        
        return nonEmptyUnits;
    }
}
