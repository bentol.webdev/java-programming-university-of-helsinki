/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class Box implements Packable{
    private double capacity;
    private ArrayList<Packable> contents;

    public Box(double capacity) {
        this.contents = new ArrayList<>();
        this.capacity = capacity;
    }
    
    public void add(Packable item) {
        if(this.weight() + item.weight() > this.capacity){
            return;
        }
        contents.add(item);
    }

    @Override
    public double weight() {
        return contents
                .stream()
                .map(packable -> packable.weight())
                .reduce(0.0, Double::sum);
    }

    @Override
    public String toString() {
        return "Box: " + this.contents.size() + " items, total weight " + this.weight() + " kg";
    }
    
}
