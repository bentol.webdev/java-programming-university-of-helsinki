
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class BooksFromFile {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // test your method here
        try {
            Files.lines(Paths.get("sample.txt"))
                    .map(row -> row.split(","))
                    .filter(row -> row.length >= 4)
                    .map(parts -> {
//                        System.out.println(Arrays.toString(parts));
                        String name = parts[0];
                        int year = Integer.valueOf(parts[1]);
                        int pages = Integer.valueOf(parts[2]);
                        String author = parts[3];
                        return new Book(name, year, pages, author);
                    })
                    .collect(Collectors.toList())
                    .forEach(b -> System.out.println(b.getName()));
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static List<Book> readBooks(String file) {
        try {
            return Files.lines(Paths.get(file))
                    .map(row -> row.split(","))
                    .filter(row -> row.length >= 4)
                    .map(parts -> {
                        String name = parts[0];
                        int year = Integer.valueOf(parts[1]);
                        int pages = Integer.valueOf(parts[2]);
                        String author = parts[3];
                        return new Book(name, year, pages, author);
                    })
                    .collect(Collectors.toList());
        } catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        
        return new ArrayList<>();
    }

}
