
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AverageOfNumbers {

    public static void main(String[] args) {
//        String inputString = "2\n4\n6\nend";
//        String inputString = "-1\n1\n2\nend";
//        Scanner scanner = new Scanner(inputString);
        Scanner scanner = new Scanner(System.in);
        // Write your program here

        List<String> inputs = new ArrayList<>();

        System.out.println("Input numbers, type \"end\" to stop.");
        while (true) {
            String input = scanner.nextLine();

            if (input.equals("end")) {
                break;
            }

            inputs.add(input);
        }

        System.out.println("average of the numbers:");
        double average = inputs.stream().mapToInt(i -> Integer.valueOf(i)).average().getAsDouble();
        System.out.println(average);
    }
}
