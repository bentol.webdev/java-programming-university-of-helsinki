
import java.util.Objects;

public class LicensePlate {
    // don't modify existing parts of this class

    // these instance variables have been defined as final, meaning 
    // that once set, their value can't be changed
    private final String liNumber;
    private final String country;

    public LicensePlate(String country, String liNumber) {
        this.liNumber = liNumber;
        this.country = country;
    }

    @Override
    public String toString() {
        return country + " " + liNumber;
    }
    
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        
        if(o == null || this.getClass() != o.getClass()){
            return false;
        }
        
        LicensePlate compared = (LicensePlate) o;
        
        return this.liNumber.equals(compared.liNumber) &&
                this.country.equals(compared.country);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.liNumber);
        hash = 83 * hash + Objects.hashCode(this.country);
        return hash;
        
    }
    
    
   
}
