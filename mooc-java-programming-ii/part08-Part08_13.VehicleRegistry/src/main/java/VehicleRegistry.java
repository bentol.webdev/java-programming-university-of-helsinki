/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.HashMap;
import java.util.HashSet;

public class VehicleRegistry {
    private HashMap<LicensePlate, String> registry;
    
    public VehicleRegistry() {
        registry = new HashMap<>();
    }
    
    public boolean add(LicensePlate lp, String owner){
        if(registry.containsKey(lp)){
            return false;
        }
        
        registry.put(lp, owner);
        return true;
    }
    
    public String get(LicensePlate lp) {
        return registry.get(lp);
    }
    
    public boolean remove(LicensePlate lp) {
        if(registry.containsKey(lp)){
            registry.remove(lp);
            return true;
        }
        
        return false;
    }
    
    public void printLicensePlates() {
        registry.keySet().forEach(lp -> {
            System.out.println(lp);
        });
    }
    
    public void printOwners() {
        HashSet<String> printedOwners = new HashSet<>();
        
        registry.values().forEach(owner -> {
            if(!printedOwners.contains(owner)){
                System.out.println(owner);
                printedOwners.add(owner);
            }
        });
        
    }
}
