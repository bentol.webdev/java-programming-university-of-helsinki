
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LimitedNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();
        
        while(true) {
            int num = Integer.valueOf(scanner.nextLine());
            
            if(num < 0) {
                break;
            }
            
            numbers.add(num);
        }
        
        numbers
            .stream()
            .filter(i -> i >= 1 && i <= 5)
            .forEach(i -> System.out.println(i));
    }
}
