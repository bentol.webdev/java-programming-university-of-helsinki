/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class ChangeHistory {
    private ArrayList<Double> priceHistory;

    public ChangeHistory() {
        this.priceHistory = new ArrayList<>();
    }
    
    public void add(double status) {
        priceHistory.add(status);
    }
    
    public void clear() {
        priceHistory.clear();
    }
    
    public String toString() {
        return priceHistory.toString();
    }
    
    public double maxValue() {
        double max = 0;
        max = priceHistory.stream().reduce(max, Double::max);
        return max;
    }
    
    public double minValue() {
        double min = Double.MAX_VALUE;
        min = priceHistory.stream().reduce(min, Double::min);
        return min;
    }
    
    public double average() {
        if(priceHistory.isEmpty()){
            return 0;
        }
        
        double sum = 0;
        sum = priceHistory.stream().reduce(sum, Double::sum);
        return 1.0 * sum / priceHistory.size();
    }
}
