/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class OneItemBox extends Box{
    private Item contents;
    
    @Override
    public void add(Item item) {
        if(contents != null) {
            return;
        }
        
        contents = item;
    }

    @Override
    public boolean isInBox(Item item) {
        if(contents == null) {
            return false;
        }
        
        return contents.equals(item);
    }
    
    
}
