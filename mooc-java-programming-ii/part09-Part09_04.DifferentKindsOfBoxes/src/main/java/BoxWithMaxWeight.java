/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class BoxWithMaxWeight extends Box{
    private int capacity;
    private ArrayList<Item> contents;

    public BoxWithMaxWeight(int capacity) {
        contents = new ArrayList<>();
        this.capacity = capacity;
    }

    @Override
    public void add(Item item) {
        if(this.getWeight() + item.getWeight() > this.capacity) {
            return;
        }
        
        contents.add(item);
    }

    @Override
    public boolean isInBox(Item item) {
        return contents.contains(item);
    }
    
    public int getWeight() {
        int sum = 0;
        sum = contents.stream().map(item -> item.getWeight()).reduce(sum, Integer::sum);
        return sum;
    }
    
    
}
