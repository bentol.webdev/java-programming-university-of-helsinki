/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.HashMap;

public class Abbreviations {

    HashMap<String, String> directory;

    public Abbreviations() {
        directory = new HashMap<>();
    }

    public void addAbbreviation(String abbreviation, String explanation) {
        directory.put(sanitizedString(abbreviation), explanation);
    }

    public boolean hasAbbreviation(String abbreviation) {
        return directory.containsKey(sanitizedString(abbreviation));
    }

    public String findExplanationFor(String abbreviation) {

        String abbr = sanitizedString(abbreviation);

        if (directory.containsKey(abbr)) {
            return directory.get(abbr);
        }
        
        return null;

    }

    private String sanitizedString(String string) {
        if (string == null) {
            return "";
        }

        String result = string.toLowerCase();
        return result.trim();
    }
}
