/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;

public class UserInterface {
    private Scanner scanner;
    private TodoList todoList;
    
    public UserInterface(TodoList t, Scanner sc) {
        this.scanner = sc;
        this.todoList = t;
    }
    
    public void start() {
        while(true) {
            System.out.println("Command:");
            String input = scanner.nextLine();
            
            if(input.equals("stop")){
                break;
            }
            
            processInput(input);
        }
    }
    
    private void processInput(String input) {
        switch(input) {
            case "add":
                add();
                break;
            case "list":
                list();
                break;
            case "remove":
                remove();
                break;
        }
    }
    
    private void add() {
        System.out.println("To add: ");
        String task = scanner.nextLine();
        todoList.add(task);
    }
    
    private void list() {
        todoList.print();
    }
    
    private void remove() {
        System.out.println("Which one is removed? ");
        int taskNumber = Integer.valueOf(scanner.nextLine());
        todoList.remove(taskNumber);
    }
}
