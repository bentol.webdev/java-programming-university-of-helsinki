/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class TodoList {
    private ArrayList<String> todos;
    
    public TodoList() {
        todos = new ArrayList<>();
    }
    
    public void add(String task){
        todos.add(task);
    }
    
    public void print() {
        for(int i = 0; i < todos.size(); i++) {
            System.out.println((i + 1) + ": " + todos.get(i));
        }
    }
    
    public void remove(int taskNumber) {
        if(taskNumber > todos.size() || taskNumber < 0) {
            return;
        }
        
        todos.remove(taskNumber - 1);
    }
}
