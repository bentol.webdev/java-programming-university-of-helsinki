
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class Herd implements Movable{
    private List<Movable> organisms;
    
    public Herd() {
        organisms = new ArrayList<>();
    }
    
    public void addToHerd(Movable movable){
        organisms.add(movable);
    }
    
    @Override
    public void move(int dx, int dy) {
        organisms.stream().forEach(o -> o.move(dx, dy));
    }

    @Override
    public String toString() {
        return organisms
                .stream()
                .map(m -> m.toString() +"\n")
                .reduce("", String::concat);
    }

    
}
