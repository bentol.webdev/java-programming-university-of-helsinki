
import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class ShoppingCart {
    private Map<String, Item> cartItems;

    public ShoppingCart() {
        cartItems = new HashMap<>();
    }
    
    public void add(String product, int price) {
//        if(cartItems.containsKey(product)){
//            cartItems.get(product).increaseQuantity();
//            return;
//        }
//        
//        cartItems.put(product, new Item(product, 1, price));
        this.cartItems.putIfAbsent(product, new Item(product, 0, price));
        this.cartItems.get(product).increaseQuantity();
    }
    
    public int price() {
        return cartItems
                .values()
                .stream()
                .map(i -> i.price())
                .reduce(0, Integer::sum);
    }
    
    public void print() {
        cartItems
                .values()
                .forEach(item -> {
                    System.out.println(item);
                });
    }
}
