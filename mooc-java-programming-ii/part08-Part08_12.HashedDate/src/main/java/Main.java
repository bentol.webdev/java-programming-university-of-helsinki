import java.util.HashMap;
public class Main {

    public static void main(String[] args) {
        // make test programs here
        SimpleDate date1 = new SimpleDate(1, 2, 2023);
        SimpleDate date2 = new SimpleDate(1, 3, 2024);
        
        HashMap<SimpleDate, String> dateTable= new HashMap<>();
        
        dateTable.put(date1, date1.toString());
        System.out.println(dateTable.get(date1));
        System.out.println(dateTable.get(new SimpleDate(1, 2, 2023) ));
        System.out.println(dateTable.containsKey(new SimpleDate(1, 2, 2023) ));
        System.out.println(dateTable.get(date2));
        System.out.println(date1.hashCode());
    }
}
