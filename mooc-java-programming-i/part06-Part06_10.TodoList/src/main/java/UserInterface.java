/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;

public class UserInterface {
    private TodoList todos;
    private Scanner scanner;
    
    public UserInterface(TodoList todos, Scanner scanner) {
        this.todos = todos;
        this.scanner = scanner;
    }
    
    public void start() {
        while(true) {
            System.out.println("Command: ");
            String input = scanner.nextLine();
            
            if(input.equals("stop")){
                break;
            }
            
            processInput(input); 
        }
    }
    
    public void processInput(String input) {
        if(input.equals("add")){
            add();
        } else if(input.equals("list")) {
            list();
        } else if(input.equals("remove")) {
            remove();
        }
    }
    
    public void add() {
        System.out.println("To add: ");
        String task = scanner.nextLine();
        todos.add(task);
    }
    
    public void list() {
        todos.print();
    }
    
    public void remove() {
        System.out.println("Which one is removed? ");
        int taskNumber = Integer.valueOf(scanner.nextLine());
        todos.remove(taskNumber);
    }
}
