/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class BirdObservationDatabase {
    private ArrayList<Bird> birds;
    private ArrayList<String> observations;
    
    public BirdObservationDatabase() {
        this.birds = new ArrayList<>();
        this.observations = new ArrayList<>();
    }
    
    public void addBird(Bird bird) {
        birds.add(bird);
    }
    
    public boolean hasBird(String name) {
        return birds.contains(new Bird(name, ""));
    }
    
    public void addObservation(String bird) {
        observations.add(bird);
    }
    
    public int getBirdObservations(String name) {
        int count = 0;
        for(String bird : observations) {
            //System.out.println(name + " == " + bird);
            if (name.equals(bird)){
                count++;
            }
        }
        return count;
    }
    
    public String getOneBird(String name) {
        String output = "";
        for(Bird bird : birds) {
            if(bird.getName().equals(name)){
                output += bird + ": " + getBirdObservations(name) + " observations";
            }
        }
        return output;
    }
    
    public String getAllBirds(){
        String output = "";
        for(Bird bird : birds) {
            String name = bird.getName();
            int observationCount = getBirdObservations(name);
            output += bird.toString() +": " + observationCount +"\n";
            
        }
        
        return output;
    }
}
