
import java.util.Scanner;

public class mainProgram {

    public static void main(String[] args) {
        // NB! Do not create other scanner objects than the one below
        // if and when you create other classes, pass the scanner to them
        // as a parameter

//        String input = "Add\nCrow\nCorvus Corvus\nAdd\nHawk\nDorkus Dorkus\nObservation\nHawk\nObservation\nLion\nObservation\nHawk\nAll\nOne\nHawk\nQuit";
//        Scanner scanner = new Scanner(input);
        Scanner scanner = new Scanner(System.in);
        BirdObservationDatabase bodb = new BirdObservationDatabase();
        UserInterface ui = new UserInterface(scanner, bodb);
        
        ui.start();
    }

}
