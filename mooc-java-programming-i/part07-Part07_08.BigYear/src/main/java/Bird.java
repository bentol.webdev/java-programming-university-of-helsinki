/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class Bird {

    private String name;
    private String latinName;

    public Bird(String name, String latinName) {
        this.name = name;
        this.latinName = latinName;
    }
    
    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name + " " + "(" + this.latinName + ")";
    }
    
    public boolean equals(Object compared) {
        if(compared == null) {
            return false;
        }
        
        if(this.getClass() != compared.getClass()) {
            return false;
        }
        
        Bird comparedBird = (Bird) compared;
        if(this.name.equals(comparedBird.name)) {
            return true;
        }
        
        return false;
    }
}
