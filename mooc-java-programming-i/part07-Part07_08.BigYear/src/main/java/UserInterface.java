/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;

public class UserInterface {

    private Scanner scanner;
    private BirdObservationDatabase bodb;

    public UserInterface(Scanner scanner, BirdObservationDatabase bodb) {
        this.scanner = scanner;
        this.bodb = bodb;
    }

    public void start() {
        printCommands();

        while (true) {
            System.out.println("?");
            String input = scanner.nextLine();
            
            if(input.equals("Quit")){
                break;
            }
            
            processCommand(input);
        }
    }

    private void printCommands() {
        System.out.println("Commands:");
        System.out.println("Add - adds a bird");
        System.out.println("Observation - adds an observation");
        System.out.println("All - prints all birds");
        System.out.println("One - prints one bird");
        System.out.println("Quit - ends the program");
    }
    
    private void processCommand(String input) {
        switch(input) {
            case "Add":
                add();
                break;
            case "Observation":
                addObservation();
                break;
            case "All":
                printAllBirds();
                break;
            case "One":
                printOneBird();
                break;
        }
    }
    
    private void add() {
        System.out.println("Name:");
        String name = scanner.nextLine();
        System.out.println("Name in Latin: ");
        String latinName = scanner.nextLine();
        
        bodb.addBird(new Bird(name, latinName));
    }
    
    private void printAllBirds() {
        System.out.println(bodb.getAllBirds());
    }
    
    private void printOneBird() {
        System.out.println("Bird?");
        String birdName = scanner.nextLine();
        System.out.println(bodb.getOneBird(birdName));
    }
    
    private void addObservation() {
        System.out.println("Bird?");
        String birdName = scanner.nextLine();
        
        if(!bodb.hasBird(birdName)) {
            System.out.println("Not a bird!");
            return;
        }
        
        bodb.addObservation(birdName);
    }
}
