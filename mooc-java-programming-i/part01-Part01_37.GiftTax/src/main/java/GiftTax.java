
import java.util.Scanner;

public class GiftTax {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Value of the gift?");
        int giftValue = Integer.valueOf(scan.nextLine());
        int taxAtLowerLimit;
        int taxRate;
        int lowerLimit;

        if (giftValue >= 1000000) {
            System.out.println("top bracket");
            taxAtLowerLimit = 142100;
            lowerLimit = 1000000;
            taxRate = 17;
        } else if (giftValue >= 200000) {
            taxAtLowerLimit = 22100;
            lowerLimit = 200000;
            taxRate = 15;
        } else if (giftValue >= 55000) {
            taxAtLowerLimit = 4700;
            lowerLimit = 55000;
            taxRate = 12;
        } else if (giftValue >= 25000) {
            taxAtLowerLimit = 1700;
            lowerLimit = 25000;
            taxRate = 10;
        } else if (giftValue >= 5000) {
            taxAtLowerLimit = 100;
            lowerLimit = 5000;
            taxRate = 8;
        } else {
            taxAtLowerLimit = 0;
            lowerLimit = 0;
            taxRate = 0;
        }

        if (giftValue < 5000) {
            System.out.println("No tax!");
        } else {
            double giftTax = taxAtLowerLimit + (giftValue - lowerLimit) * (1.0 * taxRate / 100);
            System.out.println("Tax: " + giftTax);
        }

    }
}
