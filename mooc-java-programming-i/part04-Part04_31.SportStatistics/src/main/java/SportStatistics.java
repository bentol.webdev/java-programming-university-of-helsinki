
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class SportStatistics {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("File:");
        String file = scan.nextLine();
        System.out.println("Team: ");
        String searchFor = scan.nextLine();

        ArrayList<SportStatistic> stats = new ArrayList<>();

        //get statistics from file and add to arraylist
        try (Scanner fileScanner = new Scanner(Paths.get(file))) {
            while (fileScanner.hasNextLine()) {
                String[] pieces = fileScanner.nextLine().split(",");
                String homeTeam = pieces[0];
                String visitorTeam = pieces[1];
                int homePoints = Integer.valueOf(pieces[2]);
                int visitorPoints = Integer.valueOf(pieces[3]);
                stats.add(new SportStatistic(homeTeam, visitorTeam, homePoints, visitorPoints));
            }
        } catch (Exception e) {
            System.out.println("Error readign " + file);
        }
        
        int gamesPlayed = 0;
        int gamesWon = 0;
        int gamesLost = 0;
        for(SportStatistic stat : stats){
            String[] teams = stat.getTeams();
            if(teams[0].equals(searchFor) || teams[1].equals(searchFor)){
                gamesPlayed++;
            }
            
            if(stat.getWinningTeam().equals(searchFor)){
                gamesWon++;
            }
            
            if(stat.getLosingTeam().equals(searchFor)){
                gamesLost++;
            }
        };
        System.out.println("Games: " + gamesPlayed);
        System.out.println("Wins: " + gamesWon);
        System.out.println("Losses: " + gamesLost);
    }

}
