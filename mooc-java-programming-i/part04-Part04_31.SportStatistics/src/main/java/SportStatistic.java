/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class SportStatistic {
    private String homeTeam;
    private String visitorTeam;
    private int homePoints;
    private int visitorPoints;
    
    public SportStatistic (String homeTeam, String visitorTeam, int homePoints, int visitorPoints) {
        this.homeTeam = homeTeam;
        this.visitorTeam = visitorTeam;
        this.homePoints = homePoints;
        this.visitorPoints = visitorPoints;
    }
    
    public String[] getTeams() {
        String[] teams = {this.homeTeam, this.visitorTeam};
        return teams;
    }
    
    public String getWinningTeam() {
        if(this.homePoints > this.visitorPoints) {
            return this.homeTeam;
        } else {
            return this.visitorTeam;
        }
    }
    
    public String getLosingTeam() {
        if(this.homePoints > this.visitorPoints){
            return this.visitorTeam;
        } else {
            return this.homeTeam;
        }
    }
}
