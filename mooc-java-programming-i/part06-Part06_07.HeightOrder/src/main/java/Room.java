
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class Room {

    private ArrayList<Person> occupants;

    public Room() {
        this.occupants = new ArrayList<>();
    }

    public void add(Person person) {
        this.occupants.add(person);
    }

    public boolean isEmpty() {
        return this.occupants.isEmpty();
    }

    public ArrayList<Person> getPersons() {
        return this.occupants;
    }

    public Person shortest() {

        if (this.occupants.isEmpty()) {
            return null;
        }

        Person returnObject = this.occupants.get(0);

        for (Person person : occupants) {
            if (person.getHeight() < returnObject.getHeight()) {
                returnObject = person;
            }
        }
        
        return returnObject;
    }
    
    public Person take() {
        Person shortestPerson = this.shortest();
        this.occupants.remove(shortestPerson);
        
        return shortestPerson;
    }

}
