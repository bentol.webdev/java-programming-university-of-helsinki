
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Searching {

    public static void main(String[] args) {
        // The program below is meant for testing the search algorithms you'll write
        Scanner scanner = new Scanner(System.in);
        ArrayList<Book> books = new ArrayList<>();
        System.out.println("How many books to create?");
        int numberOfBooks = Integer.valueOf(scanner.nextLine());
        for (int i = 0; i < numberOfBooks; i++) {
            books.add(new Book(i, "name for the book " + i));
        }

        System.out.println("Id of the book to search for?");
        int idToSearchFor = Integer.valueOf(scanner.nextLine());

        System.out.println("");
        System.out.println("Searching with linear search:");
        long start = System.currentTimeMillis();
        int linearSearchId = linearSearch(books, idToSearchFor);
        System.out.println("The search took " + (System.currentTimeMillis() - start) + " milliseconds.");
        if (linearSearchId < 0) {
            System.out.println("Book not found");
        } else {
            System.out.println("Found it! " + books.get(linearSearchId));
        }

        System.out.println("");

        System.out.println("");
        System.out.println("Seaching with binary search:");
        start = System.currentTimeMillis();
        int binarySearchId = binarySearch(books, idToSearchFor);
        System.out.println("The search took " + (System.currentTimeMillis() - start) + " milliseconds.");
        if (binarySearchId < 0) {
            System.out.println("Book not found");
        } else {
            System.out.println("Found it! " + books.get(binarySearchId));
        }
//         
//         
//         
//[(id: 384712; name: name 384712), (id: 434439; name: name 434439), (id: 620477; name: name 620477)]
//        ArrayList<Book> books = new ArrayList<>();
//        books.add(new Book(384712, "name 384712"));
//        books.add(new Book(434439, "name 434439"));
//        books.add(new Book(620477, "name 620477"));
        
        for(int i = 0; i < books.size(); i++) {
            
        }
    }

    public static int linearSearch(ArrayList<Book> books, int searchedId) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getId() == searchedId) {
                return i;
            }
        }

        return -1;
    }

    public static int binarySearch(ArrayList<Book> books, long searchedId) {
        //System.out.println("Binary search for " + books.size() + " books, searching for " + searchedId);
//        if(books.size() == 2){
//            System.out.println("******BOOK SIZE 2*************BOOK SIZE 2*************BOOK SIZE 2*******");
//        }
        int begin = 0;
        int end = books.size() - 1;
        int middle;
        int middleId;
        while(true) {
            
            middle = (begin + end) / 2;
            middleId = books.get(middle).getId();
            
            //System.out.println("begin: " + begin + ", middle: " + middle + ", end: " + end);
            
            if(begin > end) {
                //System.out.println("begin > end");
                break;
            }
            
            if(middleId == searchedId) {
                //System.out.println("FOUND " + searchedId + ", at index " + middle);
                return middle;
            } else if(middleId > searchedId) {
                end = middle - 1;
            } else if (middleId < searchedId) {
                begin = middle + 1;
            }
        }
        
        //System.out.println("CANNOT FIND book with id " + searchedId);
        return -1;
    }

}
