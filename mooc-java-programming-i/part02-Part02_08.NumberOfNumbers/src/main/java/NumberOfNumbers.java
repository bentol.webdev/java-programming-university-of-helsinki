
import java.util.Scanner;

public class NumberOfNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int countNumbers = 0;
        int input;
        while (true) {
            System.out.println("Give a number:");
            input = Integer.valueOf(scanner.nextLine());
            
            if(input == 0) {
                break;
            }
            
            countNumbers++;
        }
        System.out.println("Number of numbers: " + countNumbers);
    }
}
