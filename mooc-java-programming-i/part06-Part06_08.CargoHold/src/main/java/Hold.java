/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class Hold {

    private int weightLimit;
    private ArrayList<Suitcase> suitcases;

    public Hold(int weightLimit) {
        this.weightLimit = weightLimit;
        this.suitcases = new ArrayList<>();
    }

    public void addSuitcase(Suitcase suitcase) {
        if (this.totalWeight() + suitcase.totalWeight() > this.weightLimit ) {
            return;
        }
        
        suitcases.add(suitcase);
    }

    public int totalWeight() {
        int totalWeight = 0;

        for (Suitcase s : suitcases) {
            totalWeight += s.totalWeight();
        }
        
        return totalWeight;
    }
    
    public void printItems() {
        for(Suitcase s : suitcases) {
            s.printItems();
        }
    }
    
    public String toString() {
        String output = this.suitcases.size() + " suitcases (" + this.totalWeight() + " kg)";
        return output;
    }
}
