/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class Suitcase {

    private ArrayList<Item> items;
    private int weightLimit;

    public Suitcase(int weightLimit) {
        this.items = new ArrayList<>();
        this.weightLimit = weightLimit;
    }

    public void addItem(Item item) {
        if ((this.totalWeight() + item.getWeight()) > this.weightLimit) {
            return;
        }

        items.add(item);

    }

    public int totalWeight() {
        int totalWeight = 0;

        for (Item item : this.items) {
            totalWeight += item.getWeight();
        }

        return totalWeight;
    }
    
    public void printItems() {
        for(Item item : items) {
            System.out.println(item);
        }
    }

    public String toString() {
        String output = "";
        
        if (items.isEmpty()) {
            return "no items (0 kg)";
        } else if (items.size() == 1) {
            output += "1 item (";
        } else {
            output += items.size() + " items (";
        }
        
        return output + this.totalWeight() + " kg)";
    }
    
    public Item heaviestItem() {
        if(items.isEmpty()) {
            return null;
        }
        
        Item returnObject = this.items.get(0);
        for(Item item : items) {
            if(item.getWeight() > returnObject.getWeight()) {
                returnObject = item;
            }
        }
        
        return returnObject;
    }
}
