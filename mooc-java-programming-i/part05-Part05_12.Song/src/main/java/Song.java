
public class Song {

    private String artist;
    private String name;
    private int durationInSeconds;

    public Song(String artist, String name, int durationInSeconds) {
        this.artist = artist;
        this.name = name;
        this.durationInSeconds = durationInSeconds;
    }
    
    @Override
    public boolean equals(Object compared) {
        // check if this and compared point to the same reference
        if(this == compared){
            return true;
        }
        
        // check if compared is not a Song object
        if( !(compared instanceof Song) ){
            return false;
        }
        
        //convert compared to a Song object
        Song comparedSong = (Song) compared;
        
        //check if this and compared have equal instance variables.
        if(this.artist.equals(comparedSong.artist) &&
           this.name.equals(comparedSong.name) &&
           this.durationInSeconds == comparedSong.durationInSeconds){
            return true;
        }
        
        return false;
    }

    @Override
    public String toString() {
        return this.artist + ": " + this.name + " (" + this.durationInSeconds + " s)";
    }


}
