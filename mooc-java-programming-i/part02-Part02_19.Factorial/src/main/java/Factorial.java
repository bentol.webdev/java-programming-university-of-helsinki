
import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Give a number: ");
        int num = Integer.valueOf(scanner.nextLine());
        long factorial = 1;
        int index = 1;
        
        while(index <= num) {
            if(num == 0 || num == 1) {
                break;
            }
            
            factorial *= index;
            index++;
        }
        
        System.out.println("Factorial: " + factorial);
    }
}
