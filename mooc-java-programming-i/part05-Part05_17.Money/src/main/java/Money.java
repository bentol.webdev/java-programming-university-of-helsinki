
public class Money {

    private final int euros;
    private final int cents;

    public Money(int euros, int cents) {

        if (cents > 99) {
            euros = euros + cents / 100;
            cents = cents % 100;
        }

        this.euros = euros;
        this.cents = cents;
    }

    public int euros() {
        return this.euros;
    }

    public int cents() {
        return this.cents;
    }

    public String toString() {
        String zero = "";
        if (this.cents < 10) {
            zero = "0";
        }

        return this.euros + "." + zero + this.cents + "e";
    }

    public Money plus(Money addition) {
        int euros = this.euros + addition.euros();
        int cents = this.cents + addition.cents();

        return new Money(euros, cents);
    }

    public boolean lessThan(Money compared) {
        return (this.euros < compared.euros()
                || ((this.euros == compared.euros()) && this.cents < compared.cents()));
    }

    public Money minus(Money decreaser) {
        if(this.lessThan(decreaser)){
            return new Money(0,0);
        }
        
        int cents = this.cents;
        int euros = this.euros;
        if (decreaser.cents() > cents) {
            euros -= 1;
            cents += 100;
        }
        
        cents -= decreaser.cents();
        euros -= decreaser.euros();
        
        return new Money(euros, cents);
    }

}
