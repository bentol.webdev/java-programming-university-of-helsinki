
public class AdvancedAstrology {

    public static void printStars(int number) {
        // part 1 of the exercise
        while (number > 0) {
            System.out.print("*");
            number--;
        }
        System.out.println("");
    }

    public static void printSpaces(int number) {
        // part 1 of the exercise
        while (number > 0) {
            System.out.print(" ");
            number--;
        }
    }

    public static void printTriangle(int size) {
        // part 2 of the exercise
        int level = 1;
        while (level <= size) {
            printSpaces(size - level);
            printStars(level);
            level++;
        }
    }

    public static void christmasTree(int height) {
        // part 3 of the exercise
        int level = 1;

        while (level <= height) {
            printSpaces(height - level);
            printStars(2 * level - 1);
            level++;
        }

        //build base of tree
        printSpaces(level - 3);
        printStars(3);
        printSpaces(level - 3);
        printStars(3);
    }

    public static void main(String[] args) {
        // The tests are not checking the main, so you can modify it freely.

        printTriangle(5);
        System.out.println("---");
        christmasTree(4);
        System.out.println("---");
        christmasTree(10);
    }
}
