/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;

public class UserInterface {

    private GradeStatistics register;
    private Scanner scanner;

    public UserInterface(GradeStatistics register, Scanner scanner) {
        this.register = register;
        this.scanner = scanner;
    }

    public void start() {
        while (true) {
            System.out.println("Enter point totals, -1 stops:");
            String input = scanner.nextLine();
            //int point = -1;
            try {
                int point = Integer.valueOf(input);

                if (point == -1) {
                    break;
                }

                add(point);

            } catch (Exception e) {
                System.out.println("Please enter a valid number!");
            }
        }
        
        printOutput();
    }

    public void add(int point) {
        register.add(point);
    }
    
    public void printGradeDistribution() {
        register.printGradeDistribution();
    }

    public void printOutput() {
        System.out.println("Point average (all): " + register.getPointsAverage());
        
        String passingPointsAve;
        
        if (register.getPassingPointsAverage() == -1) {
            passingPointsAve = "-";
        } else {
            passingPointsAve = "" + register.getPassingPointsAverage();
        }

        System.out.println("Point average (passing): " + passingPointsAve);
        System.out.println("Pass percentage: " + register.getPassingPercentage());
        System.out.println("Grade distribution: ");
        printGradeDistribution();
    }

}
