/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class GradeStatistics {

    private ArrayList<Integer> pointsList;

    public GradeStatistics() {
        pointsList = new ArrayList<>();
    }

    public void add(int points) {
        if (points < 0 || points > 100) {
            return;
        }

        this.pointsList.add(points);
    }

    public double getPointsAverage() {
        int sum = 0;
        sum = pointsList.stream().reduce(sum, Integer::sum);
        return 1.0 * sum / pointsList.size();
    }

    public double getPassingPointsAverage() {
        int sum = 0;
        int count = getPassingPointsCount();

        if (count == 0) {
            return -1;
        }

        for (Integer points : pointsList) {
            if (points >= 50) {
                sum += points;
            }
        }

        if (count == 0) {
            return -1;
        }
        return 1.0 * sum / count;
    }

    public double getPassingPercentage() {
        return 100.0 * getPassingPointsCount() / pointsList.size();
    }

    public int getPassingPointsCount() {
        int count = 0;
        count = pointsList
                .stream()
                .filter(points -> (points >= 50))
                .map(_item -> 1)
                .reduce(count, Integer::sum);
        return count;
    }

    private int convertToGrade(int points) {
        int grade = 0;
        if (points >= 90) {
            grade = 5;
        } else if (points >= 80) {
            grade = 4;
        } else if (points >= 70) {
            grade= 3;
        } else if (points >= 60) {
            grade = 2;
        } else if (points >= 50) {
            grade = 1;
        } else {
            grade = 0;
        }
        
        //System.out.println(points + " -> " + grade);
        return grade;
    }

    public void printGradeDistribution() {
        HashMap<Integer, Integer> gradeDistribution = new HashMap<>();

        for (Integer points : pointsList) {
            int grade = convertToGrade(points);
            gradeDistribution.computeIfPresent(grade, (k, v) -> v + 1);
            gradeDistribution.computeIfAbsent(grade, k -> 1);
        }
        int starsCount;
        for (int i = 5; i >= 0; i--) {
            starsCount = gradeDistribution.containsKey(i) ? gradeDistribution.get(i) : 0;
            System.out.println(i + ": " + stars(starsCount));
        }
    }

    private String stars(int n) {
        String star = "*";
        return String.join("", Collections.nCopies(n, star));
    }

}
