
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        
        String input = "102\n-4\n33\n77\n99\n1\n-1";
//          String input = "44\n12\n81\n29\n70\n-1";
//        Scanner scanner = new Scanner(input);
        Scanner scanner = new Scanner(System.in);
        GradeStatistics register = new GradeStatistics();
        
        // Write your program here -- consider breaking the program into 
        // multiple classes.
        
        UserInterface ui = new UserInterface(register, scanner);
        ui.start();
    }
}
