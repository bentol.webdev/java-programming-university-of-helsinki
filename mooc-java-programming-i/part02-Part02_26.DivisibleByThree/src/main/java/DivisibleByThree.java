
import java.util.Scanner;

public class DivisibleByThree {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        divisibleByThreeInRange(3, 9);
    }
    
    public static void divisibleByThreeInRange(int start, int end) {
        int index = start;
        
        while (index <= end) {
            if(index % 3 == 0) {
                System.out.println(index);
            }
            index++;
        }
    }

}
