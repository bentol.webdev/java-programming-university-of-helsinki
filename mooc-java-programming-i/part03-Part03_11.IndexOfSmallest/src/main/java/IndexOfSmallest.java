
import java.util.ArrayList;
import java.util.Scanner;

public class IndexOfSmallest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // implement here a program that reads user input
        // until the user enters 9999
        ArrayList<Integer> numbers = new ArrayList<>();
        int input;
        while (true) {
            input = Integer.valueOf(scanner.nextLine());

            if (input == 9999) {
                break;
            }

            numbers.add(input);
        }

        int min = Integer.MAX_VALUE;
        // find the smallest number in list
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) < min) {
                min = numbers.get(i);
            }
        }
        System.out.println("");
        System.out.println("Smallest number: " + min);

        // find the indices where the value == min
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) == min) {
                System.out.println("Found at index: " + i);
            }
        }

        // after that, the program prints the smallest number
        // and its index -- the smallest number
        // might appear multiple times
    }
}
