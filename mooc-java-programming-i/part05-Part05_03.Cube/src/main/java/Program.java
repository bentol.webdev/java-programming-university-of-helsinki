
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give the length of the side of the cube.");
        int length = Integer.valueOf(scanner.nextLine());
        // Experiment with your program here
        Cube cube = new Cube(length);
        System.out.println(cube);
    }
}
