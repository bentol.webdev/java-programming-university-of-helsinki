
import java.util.Scanner;

public class Login {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] usernames = {"alex", "emma"};
        String[] passwords = {"sunshine", "haskell"};

        System.out.println("Enter username: ");
        String username = scanner.nextLine();
        System.out.println("Enter password: ");
        String password = scanner.nextLine();

        //look for username in usernames array
        boolean found = false;
        int index = -1;
        for (int i = 0; i < usernames.length; i++) {
            if( usernames[i].equals(username)) {
                index = i;
                found = true;
            }
        }
        
        //if user is not in the array, then exit method.
        if(!found) {
            System.out.println("Incorrect username or password!");
            return;
        }
        
        // check if password matches
        if(passwords[index].equals(password)){
            System.out.println("You have successfully logged in!")
;        } else {
            System.out.println("Incorrect username or password!");
        }
        
    }
}
