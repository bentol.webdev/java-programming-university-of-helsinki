
import java.util.ArrayList;
import java.util.Scanner;

public class PersonalDetails {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String longestName = "";
        int entryCount = 0;
        int sumBirthYears = 0;
        
        while(true) {
            String input = scanner.nextLine();
            
            if(input.isEmpty()){
                break;
            }
            
            String[] entry = input.split(",");
            String name = entry[0];
            int birthYear = Integer.valueOf(entry[1]);
            entryCount ++;
            
            if (name.length() > longestName.length()) {
                longestName = name;
            }
            
            sumBirthYears += birthYear;
        }
        
        System.out.println("Longest name: " + longestName);
        System.out.println("Average of the birth years: " + ( 1.0 * sumBirthYears / entryCount));
    }
}
