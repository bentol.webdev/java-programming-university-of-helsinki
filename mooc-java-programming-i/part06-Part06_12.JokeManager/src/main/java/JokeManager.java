/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;
import java.util.Random;

public class JokeManager {
    private ArrayList<String> jokes;
    
    public JokeManager() {
        this.jokes = new ArrayList<>();
    }
    
    public void addJoke(String joke)  {
        this.jokes.add(joke);
    }
    
    public String drawJoke() {
        if(jokes.isEmpty()) {
            return "Jokes are in short supply.";
        }
        
        Random draw = new Random();
        int randomIndex = draw.nextInt(jokes.size());
        //System.out.println("randomIndex: " + randomIndex);
        return jokes.get(randomIndex);
    }
    
    public void printJokes() {
        jokes.forEach(System.out::println); 
        //jokes.stream().forEach(System.out::println);
    }
}
