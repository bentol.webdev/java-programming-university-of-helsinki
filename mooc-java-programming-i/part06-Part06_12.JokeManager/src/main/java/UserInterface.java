/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;

public class UserInterface {
    private Scanner scanner;
    private JokeManager manager;
    
    public UserInterface(JokeManager manager, Scanner scanner) {
        this.manager = manager;
        this.scanner = scanner;
    }
    
    public void start (){
        while(true) {
            
            printCommands();
            String input = scanner.nextLine();
            
            if (input.equals("X")) {
                break;
            }
            
            processInput(input);
        }
    }
    
    public void printCommands() {
        System.out.println("Commands:");
        System.out.println("1 - add a joke");
        System.out.println("2 - draw a joke");
        System.out.println("3 - list jokes");
        System.out.println("X - stop");
    }
    
    public void processInput(String input) {
        if(input.equals("1")) {
            addJoke();
        } else if(input.equals("2")) {
            drawJoke();
        } else if(input.equals("3")) {
            listJokes();
        }
    }
    
    private void addJoke() {
        System.out.println("Write the joke to be added:");
        String joke = scanner.nextLine();
        manager.addJoke(joke);
    }
    
    private void drawJoke() {
        System.out.println("Here is a random joke: ");
        System.out.println(manager.drawJoke());;
    }
    
    private void listJokes() {
        System.out.println("Printing the jokes.");
        manager.printJokes();
    }
}
