
import java.util.Scanner;

public class NumberOfNegativeNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countNegative = 0;
        int input;

        while (true) {
            System.out.println("Give a number:");
            input = Integer.valueOf(scanner.nextLine());

            if (input == 0) {
                break;
            }

            if (input < 0) {
                countNegative++;
            }
        }

        System.out.println("Number of negative numbers: " + countNegative);
    }
}
