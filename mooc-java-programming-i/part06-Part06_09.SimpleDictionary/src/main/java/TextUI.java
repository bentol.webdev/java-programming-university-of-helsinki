/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 *
 * @author Ben
 */
public class TextUI {

    private Scanner scanner;
    private SimpleDictionary dictionary;

    public TextUI(Scanner sc, SimpleDictionary dict) {
        this.scanner = sc;
        this.dictionary = dict;
    }

    public void start() {
        while (true) {

            System.out.println("Command: ");
            String input = scanner.nextLine();

            if (input.equals("end")) {
                System.out.println("Bye bye!");
                break;
            }

            processCommand(input);

        }
    }

    public void processCommand(String input) {

        if (input.equals("add")) {
            add();
        } else if (input.equals("search")) {
            search();
        } else {
            System.out.println("Unknown command");
        }
    }

    public void add() {
        System.out.println("Word: ");
        String word = scanner.nextLine();
        System.out.println("Translation: ");
        String translation = scanner.nextLine();

        this.dictionary.add(word, translation);
    }

    public void search() {
        System.out.println("To be translated: ");
        String word = scanner.nextLine();
        String translation = this.dictionary.translate(word);

        if (translation == null) {
            System.out.println("Word " + word + " was not found");
            return;
        }
        System.out.println("Translation: " + translation);
    }
}
