
import java.util.Scanner;

public class LiquidContainers {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int container1 = 0;
        int container2 = 0;

        while (true) {
            System.out.println("First: " + container1 + "/100");
            System.out.println("Second: " + container2 + "/100");

            String input = scan.nextLine();
            
            if (input.equals("quit")) {
                break;
            }
            
            String[] parts = input.split(" ");
            String command = parts[0];
            int amount = Integer.valueOf(parts[1]);
            
            if(amount < 0) {
                continue;
            }
            
            switch(command) {
                case "add":
                    container1 += amount;
                    if (container1 > 100) {
                        container1 = 100;
                    }
                    break;
                case "move":
                    int amountToMove = amount;
                    
                    if(amountToMove > container1) {
                        amountToMove = container1;
                    }
                    
                    container2 += amountToMove;
                    container1 -= amountToMove;
                    
                    if(container2 > 100) {
                        container2 = 100;
                    }
                    break;
                
                case "remove":
                    int amountToRemove = amount;
                    
                    if(amountToRemove > container2) {
                        amountToRemove = container2;
                    }
                    
                    container2 -= amountToRemove;
                    break;
 
            }

        }
    }

}
