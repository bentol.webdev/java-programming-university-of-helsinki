
import java.util.Scanner;

public class LiquidContainers2 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Container container1 = new Container();
        Container container2 = new Container();

        while (true) {
            System.out.println("First: " + container1);
            System.out.println("Second: " + container2);

            String input = scan.nextLine();
            if (input.equals("quit")) {
                break;
            }
            
            String[] parts = input.split(" ");
            String command = parts[0];
            if(parts.length < 2){
                continue;
            }
            int amount = Integer.valueOf(parts[1]);
            switch (command) {
                case "add":
                    container1.add(amount);
                    break;
                case "move":
                    if(amount > container1.contains()){
                        amount = container1.contains();
                    }
                    container1.remove(amount);
                    container2.add(amount);
                    break;
                case "remove":
                    container2.remove(amount);
                    break;
                default:
                    continue;
            }

        }
    }

}
