
import java.util.Scanner;

public class AverageOfNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sum = 0;
        int count = 0;
        int num;
        
        while (true) {
            System.out.println("Give a number:");
            num = Integer.valueOf(scanner.nextLine());

            if (num == 0) {
                break;
            }

            sum += num;
            count++;
        }

        double average = 1.0 * sum / count;
        System.out.println("Average of the numbers: " + average);
    }
}
