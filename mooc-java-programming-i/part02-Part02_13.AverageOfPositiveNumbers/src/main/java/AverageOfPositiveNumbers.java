
import java.util.Scanner;

public class AverageOfPositiveNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countPositive = 0;
        int sumPositive = 0;
        int input;
        
        while (true) {
            input = Integer.valueOf(scanner.nextLine());
            
            if (input == 0) {
                break;
            }
            
            if (input > 0) {
                sumPositive += input;
                countPositive++;
            }
        }
        
        if(countPositive == 0) {
            System.out.println("Cannot calculate the average");
        } else {
            System.out.println(1.0 * sumPositive / countPositive);
        }
        
    }
}
