
import java.util.ArrayList;

public class ExerciseManagement {

    private ArrayList<Exercise> exercises;
    private ArrayList<String> completedExercises;

    public ExerciseManagement() {
        this.exercises = new ArrayList<>();
    }

    public ArrayList<String> exerciseList() {
        ArrayList<String> list = new ArrayList<>();
        exercises.forEach(exercise -> {
            list.add(exercise.getName());
        });
        return list;
    }

    public void add(String exercise) {
        this.exercises.add(new Exercise(exercise));
    }

    public void markAsCompleted(String exercise) {
        exercises.forEach(ex -> {
            if (ex.getName().equals(exercise)) {
                ex.setCompleted(true);
            }
        });
    }

    public boolean isCompleted(String exercise) {
        for (Exercise ex : exercises) {
            if (ex.getName().equals(exercise)) {
                return ex.isCompleted();
            }
        }

        return false;
    }

    public void remove(String exercise) {
        ArrayList<Exercise> removeThis = new ArrayList<>();
        removeThis.add(new Exercise(exercise));
        exercises.removeAll(removeThis);
    }

    public boolean has(String exercise) {
        return exercises.contains(new Exercise(exercise));
    }
}
