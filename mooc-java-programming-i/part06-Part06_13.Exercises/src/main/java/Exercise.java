/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class Exercise {
    private String name;
    private boolean completed;
    
    public Exercise(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
    
    public boolean isCompleted() {
        return this.completed;
    }
    
    public boolean equals(Object compared) {
        if(compared == null) {
            return false;
        }
        
        if(this.getClass() != compared.getClass()){
            return false;
        }
        
        Exercise comparedExercise = (Exercise) compared;
        
        if(this.name.equals(comparedExercise.name)){
            return true;
        }
        
        return false;
    }
}
