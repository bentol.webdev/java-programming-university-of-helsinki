
import java.util.Scanner;

public class NameOfTheOldest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String oldest = "";
        int maxAge = 0;
        while (true) {
            String input = scanner.nextLine();
            if (input.isEmpty()) {
                break;
            }

            String[] entries = input.split(",");
            int age = Integer.valueOf(entries[1]);

            if (age > maxAge) {
                maxAge = age;
                oldest = entries[0];
            }

        }
        System.out.println("Name of the oldest: " + oldest);
    }
}
