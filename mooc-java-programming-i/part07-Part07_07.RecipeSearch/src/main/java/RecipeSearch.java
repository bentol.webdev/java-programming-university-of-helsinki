
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class RecipeSearch {

    public static void main(String[] args) {
//        String input = "recipes.txt\nlist\nstop";
//        String input = "recipes.txt\nlist\nfind name\nroll\nstop";
//        String input = "recipes.txt\nfind cooking time\n30\nstop";
//        String input = "recipes.txt\nfind cooking time\n30\nfind cooking time\n15\nfind name\nroll\nstop";
//        String input = "recipes.txt\nfind cooking time\n30\nfind ingredient\nsugar\nfind ingredient\negg\nfind ingredient\ngg\nstop";
//        Scanner scanner = new Scanner(input);
        Scanner scanner = new Scanner(System.in);
        RecipeBook rb = new RecipeBook();
        
        UserInterface ui = new UserInterface(rb, scanner);
        ui.start();
        
    }

}
