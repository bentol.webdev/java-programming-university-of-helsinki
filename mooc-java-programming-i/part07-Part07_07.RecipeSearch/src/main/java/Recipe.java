/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;

public class Recipe {
    private String name;
    private int cookingTime;
    private ArrayList<String> ingredients;
    
    public Recipe() {
        ingredients = new ArrayList<>();
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getCookingTime() {
        return this.cookingTime;
    }
    
    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }
    
    public void addIngredient(String ingredient) {
        ingredients.add(ingredient);
    }
    
    public boolean hasIngredient(String ingredient) {
        return ingredients.contains(ingredient);
    }
    
    public String toString() {
        return this.name + ", " + "cooking time: " + this.cookingTime;
    }
    
}
