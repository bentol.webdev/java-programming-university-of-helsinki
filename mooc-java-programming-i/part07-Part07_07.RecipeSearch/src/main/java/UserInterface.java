/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.Scanner;
import java.nio.file.Paths;

public class UserInterface {

    private RecipeBook recipeBook;
    private Scanner scanner;

    public UserInterface(RecipeBook rb, Scanner sc) {
        this.recipeBook = rb;
        this.scanner = sc;
    }

    public void start() {
        System.out.println("Files to read: ");
        String input = scanner.nextLine();

        processFile(input);
        printCommands();

        while (true) {
            System.out.println("\nEnter command:");
            input = scanner.nextLine();

            if (input.equals("stop")) {
                break;
            }

            processCommands(input);
        }
    }

    private void processFile(String input) {
        try (Scanner fileScanner = new Scanner(Paths.get(input))) {
            String name = "";
            int cookingTime = 0;
            int counter = 0;
            while (fileScanner.hasNextLine()) {
                Recipe newRecipe = new Recipe();
                while (fileScanner.hasNextLine()) {
                    String row = fileScanner.nextLine();
                    if (row.isEmpty()) {
                        counter = 0;
                        break;
                    }

                    if (counter == 0) {
                        name = row;
                        newRecipe.setName(name);
                    } else if (counter == 1) {
                        cookingTime = Integer.valueOf(row);
                        newRecipe.setCookingTime(cookingTime);
                    } else {
                        newRecipe.addIngredient(row);
                    }

                    counter++;
                }
                recipeBook.add(newRecipe);
            }
        } catch (Exception e) {
            System.out.println("Error reading " + input);
            e.printStackTrace();
        }
    }
    
    public void printCommands() {
        System.out.println("\nCommands: ");
        System.out.println("list - lists the recipes");
        System.out.println("stop - stops the program");
        System.out.println("find name - searches recipes by name");
        System.out.println("find cooking time - searches recipes by cooking time");
        System.out.println("find ingredient - searches recipes by ingredient");
    }

    public void processCommands(String input) {
        
        switch(input) {
            case "list":
                printRecipes();
                break;
            case "find name":
                searchRecipe();
                break;
            case "find cooking time":
                searchRecipeByCookingTime();
                break;
            case "find ingredient":
                searchRecipeByIngredient();
        }
    }
    
    public void searchRecipe() {
        System.out.println("Searched word:");
        String searchFor = scanner.nextLine();
        recipeBook.searchRecipe(searchFor);
    }
    
    public void searchRecipeByCookingTime() {
        System.out.println("Max cooking time:");
        int maxCookingTime = Integer.valueOf(scanner.nextLine());
        recipeBook.searchRecipeByCookingTime(maxCookingTime);
    }

    public void searchRecipeByIngredient() {
        System.out.println("Ingredient:");
        String ingredient = scanner.nextLine();
        recipeBook.searchRecipeByIngredient(ingredient);
    }

    public void printRecipes() {
        this.recipeBook.printRecipes();
    }
}
