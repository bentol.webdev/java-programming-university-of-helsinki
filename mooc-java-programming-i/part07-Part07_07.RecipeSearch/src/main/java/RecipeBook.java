/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
import java.util.ArrayList;
import java.util.Iterator;

public class RecipeBook {
    private ArrayList<Recipe> recipes;
    
    public RecipeBook() {
        this.recipes = new ArrayList<>();
    }
    
    public void add(Recipe recipe) {
        this.recipes.add(recipe);
    }
    
    public void printRecipes() {
        System.out.println("Recipes:");
        for(Recipe recipe : recipes) {
            System.out.println(recipe);
        }
    }
    
    public void searchRecipe(String searchFor) {
        System.out.println("\nRecipes:");
        for (Recipe recipe : recipes) {
            if(recipe.getName().contains(searchFor)){
                System.out.println(recipe);
            }
        }
    }
    
    public void searchRecipeByCookingTime(int maxCookingTime) {
        System.out.println("\nRecipes:");
        for (Recipe recipe : recipes) {
            if(recipe.getCookingTime() <= maxCookingTime){
                System.out.println(recipe);
            }
        }
    }
    
    public void searchRecipeByIngredient(String ingredient) {
        System.out.println("\nRecipes:");
        for (Recipe recipe : recipes) {
            if (recipe.hasIngredient(ingredient)){
                System.out.println(recipe);
            }
        }
    }
}
